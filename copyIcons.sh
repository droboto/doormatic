#!/bin/sh
MAINDIR="/home/aensis/Documentos/Personal/Android/Icons/assets"

#ICONSET="black"
ICONSET="blue_dark"
#ICONSET="green_light"
#ICONSET="white"

DEST="/home/aensis/workspace/Doormatic/res"
#DEST="/home/aensis/workspace/HyperQR15/bin/res"

ICONS="ic_action_exit.png ic_action_info.png ic_action_settings.png ic_action_help.png ic_action_gear.png ic_action_key.png ic_action_wifi.png ic_action_lock_open.png ic_action_lock_closed.png ic_action_globe.png ic_action_monitor.png ic_action_user.png ic_action_database.png ic_action_edit.png ic_action_copy.png"


echo "Starting proccess."
for icon in ${ICONS}
do
	echo "Copying icon '$icon' to $DEST/drawable-hdpi/"
	cp $MAINDIR/$ICONSET/hdpi/$icon $DEST/drawable-hdpi/
	echo "Copying icon '$icon' to $DEST/drawable-ldpi/"
	cp $MAINDIR/$ICONSET/ldpi/$icon $DEST/drawable-ldpi/
	echo "Copying icon '$icon' to $DEST/drawable-mdpi/"
	cp $MAINDIR/$ICONSET/mdpi/$icon $DEST/drawable-mdpi/
	echo "Copying icon '$icon' to $DEST/drawable-xdpi/"
	cp $MAINDIR/$ICONSET/xhdpi/$icon $DEST/drawable-xhdpi/
	
done
echo "Process finished."

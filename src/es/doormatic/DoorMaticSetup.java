/**
 * 
 */
package es.doormatic;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author Roberto Salgado <drober+doormatic@gmail.com>
 *
 */
public class DoorMaticSetup extends Activity {
	static final String tag = "DoorMatic";
	public static final String PREFS_NAME = "DoorMatic";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);

		Log.v(tag, "Access to setup");

		EditText server		=   (EditText)findViewById(R.id.server);
		EditText username	=	(EditText)findViewById(R.id.username);
		EditText password	=	(EditText)findViewById(R.id.password);
		EditText SSID		=	(EditText)findViewById(R.id.ssid);
		CheckBox atStartup	=	(CheckBox)findViewById(R.id.openAtStart);

		//login.setText("drobouser");
		//pass.setText("prueba");

		//content.setText("Buenas, dragoncete!");

		// Setup save button properties
		Button saveme = (Button)findViewById(R.id.save);
        saveme.setOnClickListener(saveDoorSetup);

		// Load form data, if exists
		if (loadStringPreference("server") != null) {
			server.setText(loadStringPreference("server"));
		}

		if (loadStringPreference("username") != null) {
			username.setText(loadStringPreference("username"));
		}

		if (loadStringPreference("password") != null) {
			password.setText(loadStringPreference("password"));
		}

		if (loadStringPreference("SSID") != null) {
			SSID.setText(loadStringPreference("SSID"));
		}

		if (loadIntPreference("openAtStart") == 1) {
			atStartup.setChecked(true);
		}

	}
    private void msgBox(String message) {
    	Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void msgBox(String message, String reportMsg) {
    	TextView report = (TextView) findViewById(R.id.report);
    	Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    	report.setText(reportMsg);
    }
    
    private void setPreference (String preference, String value) {
        SharedPreferences.Editor editor = (SharedPreferences.Editor) getSharedPreferences(PREFS_NAME,0).edit();
        editor.putString(preference, value);
        editor.commit();
    }

    private void setPreference (String preference, int value) {
        SharedPreferences.Editor editor = (SharedPreferences.Editor) getSharedPreferences(PREFS_NAME,0).edit();
        editor.putInt(preference, value);
        editor.commit();
    }

    private String loadStringPreference (String preference) {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,0); 
        return prefs.getString(preference, null);
    }

    private int loadIntPreference (String preference) {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,0);
        return prefs.getInt(preference, -1);
    }

    private void removePreference (String preference) {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,0);
        prefs.edit().remove(preference.toString()).commit();
    }

    private final Button.OnClickListener saveDoorSetup = new Button.OnClickListener() {
        public void onClick(View v) {
    		EditText server		=   (EditText)findViewById(R.id.server);
    		EditText username	=	(EditText)findViewById(R.id.username);
    		EditText password	=	(EditText)findViewById(R.id.password);
    		EditText SSID		=	(EditText)findViewById(R.id.ssid);
    		CheckBox atStartup	=	(CheckBox)findViewById(R.id.openAtStart);

        	//msgBox("Guardando configuración");
        	setPreference("server", server.getText().toString());
        	setPreference("username", username.getText().toString());
        	setPreference("password", password.getText().toString());
        	setPreference("SSID", SSID.getText().toString());
        	if (atStartup.isChecked()) {
        		setPreference("openAtStart", 1);
        	} else {
        		setPreference("openAtStart", 0);
        	}
        	msgBox(getString(R.string.setupSaved));
        	finish();
        }
    };

}

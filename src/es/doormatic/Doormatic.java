package es.doormatic;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Doormatic extends Activity {

	static final String tag = "DoorMatic";
	public static final String PREFS_NAME = "DoorMatic";

	/*
	 * Variables for authentication
	 */
	//public int openAtStart = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doormatic);

		if (!isSetupRight()) {
			msgBox("No se detectó configuración o es incorrecta");
			showAccountSettings();
		}

		// Button "open" should call "openUpTheDoor"
		ImageButton openNow = (ImageButton)findViewById(R.id.btnOpen);
        openNow.setOnClickListener(openUpTheDoor);

		// Button "btnSettings" should call "btnSettings"
        ImageButton btnSettingsClicker = (ImageButton)findViewById(R.id.btnSettings);
        btnSettingsClicker.setOnClickListener(btnSettings);

		// Button "btnSettings" should call "btnSettings"
        ImageButton btnInfoClicker = (ImageButton)findViewById(R.id.btnInfo);
        btnInfoClicker.setOnClickListener(btnInfo);

        // get WiFi state
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Doormatic.CONNECTIVITY_SERVICE);
        Boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();

        // Check WiFi state
        if (isWifi == true) {
        	if (getWiFiData("SSID") != null) {
        		setWiFiInfo();
        	}

        	// if open at startup is set, try opening
    		if (loadIntPreference("openAtStart") == 1) {
    			openAtStart();
    		}
        } else {
            // Activity transfer to wifi settings
        	msgBox("Debes estar conectado a la red WiFi");
            //startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        	activateWiFi();
        	startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
        	finish();
        }

	}

	//@Override
	protected void onResume() {
		super.onResume();
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Doormatic.CONNECTIVITY_SERVICE);
        Boolean isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();

        // Check WiFi state
        if (isWifi == false) {
            // Activity transfer to wifi settings
        	msgBox("¡Sin WiFi no hay gloria!");
        	activateWiFi();
            //startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        	startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
        } else if (getWiFiData("SSID") != null) {
        	setWiFiInfo();
        }
	}

	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        	case R.id.action_settings:
        		// Open configuration settings
        		showAccountSettings();
        		return true;
        }
        return false;
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.doormatic, menu);
		return true;
	}

	public final String getWiFiData(String WiFiInfo) {
		String ReturnDataWiFi = null;
        WifiManager wifiManager = (WifiManager) getSystemService(Doormatic.WIFI_SERVICE);
        if (wifiManager != null) {
            final WifiInfo info = wifiManager.getConnectionInfo();
            if (info != null) {
            	if (WiFiInfo == "SSID") {
            		ReturnDataWiFi = info.getSSID();
            	} else if (WiFiInfo == "BSSID") {
            		ReturnDataWiFi = info.getBSSID();
            	} else if (WiFiInfo == "MAC") {
            		ReturnDataWiFi = info.getMacAddress();
            	}
            }
        }
		return ReturnDataWiFi;
	}

	public int getWiFiSignal() {
        WifiManager wifiManager = (WifiManager) getSystemService(Doormatic.WIFI_SERVICE);
		return wifiManager.getConnectionInfo().getRssi();
	}

	// We can activate and try to connect to our WiFi network :)
	public void activateWiFi() {
		WifiManager wifiManager = (WifiManager) getSystemService(Doormatic.WIFI_SERVICE);
		wifiManager.setWifiEnabled(true);

		//TODO: force connect to a SSID
//		if ((loadStringPreference("SSID") != null) && (!loadStringPreference("SSID").equals("")) ) {
//    		// Wifi SSID in use is retrieved as -> "SSID" <- Compare with them on!!!
//    		String RightSSID = '"' + loadStringPreference("SSID") + '"';
//    	}

		wifiManager.reconnect();
	}

    // If SSID (AP) is set it should match before trying to validate!
    private boolean isSSIDRight() {
    	// We must check if there's a saved SSID
    	if ((loadStringPreference("SSID") != null) && (!loadStringPreference("SSID").equals("")) ) {
    		// Wifi SSID in use is retrieved as -> "SSID" <- Compare with them on!!!
    		String RightSSID = '"' + loadStringPreference("SSID") + '"';
    		//Log.v(tag, "SSID [COMPOSSED]: " + RightSSID);
    		if (!getWiFiData("SSID").equals(RightSSID)) { return false; }
    	}
    	return true;
    }

	public void setWiFiInfo() {
       	TextView ssid = (TextView) findViewById(R.id.ssidName);
       	ssid.setText(getWiFiData("SSID"));
       	TextView macAddress = (TextView) findViewById(R.id.ssidMac);
       	macAddress.setText("MAC: " + getWiFiData("MAC"));
       	int linkSpeed = getWiFiSignal();
       	TextView signal = (TextView) findViewById(R.id.wifiSignal);
       	signal.setText("Señal: " + Integer.toString(linkSpeed));
	}

	// this is which should be called when the open door button is clicked
    private final Button.OnClickListener openUpTheDoor = new Button.OnClickListener() {
        public void onClick(View v) {
        	//Is setup right?
        	if (isSetupRight()) {
        		// Check if SSID is right (if should)
        		if (isSSIDRight()) {
	        		msgBox("Solicitando apertura de puerta");
		        	Post post = new Post();
		        	post.conectaPost(loadStringPreference("server"), loadStringPreference("username"), loadStringPreference("password"));
        		} else {
        			msgBox("No estás conectado a la WiFi correcta");
        		}
	        } else {
	        	msgBox("Los datos de configuración son incorrectos","Último Intento fallido");
	        	showAccountSettings();
	        }
        }

    };

    private final Button.OnClickListener btnSettings = new Button.OnClickListener() {
        public void onClick(View v) {
        	showAccountSettings();
        }
    };

    private final Button.OnClickListener btnInfo = new Button.OnClickListener() {
        public void onClick(View v) {
        	final TextView textView = new TextView(Doormatic.this);
        	//textView.setHeight(200);
        	textView.setText(R.string.infoDialogContent);
     	   	textView.setMovementMethod(LinkMovementMethod.getInstance()); // this is important to make the links clickable
     	   	final AlertDialog alertDialog = new AlertDialog.Builder(Doormatic.this)
     	   		.setTitle(R.string.dialogTitle)
     	   		.setPositiveButton("¡De acuerdo, Jack!", null)
     	   		.setView(textView)
     	   		.create();
     	   	alertDialog.show();
        }
    };

    // Check if setup is right
    private boolean isSetupRight() {
    	if ((loadStringPreference("username") == null) || (loadStringPreference("password") == null) || (loadStringPreference("server") == null) ) {
    		return false;
    	} else if ((loadStringPreference("username").equals("")) || (loadStringPreference("password").equals("")) || (loadStringPreference("server").equals("")) ) {
    		return false;
    	}
    	return true;
    }

    // Function just to open the door at start
    private final void openAtStart() {
    	//Is setup right?
    	if (isSetupRight()) {
    		// Check if SSID is right (if should)
    		if (isSSIDRight()) {
	        	msgBox("Solicitando apertura", "Solicitud realizada automáticamente (por configuración)");
	        	Post post = new Post();
	        	post.conectaPost(loadStringPreference("server"), loadStringPreference("username"), loadStringPreference("password"));
       		} else {
    			msgBox("No estás conectado a la WiFi correcta");
    		}
        }
    }

    private void showAccountSettings() {
    	msgBox("Abriendo preferencias");
		Intent intent = new Intent(this, DoorMaticSetup.class);
		startActivity(intent);
    }
    private void showDialog(int title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("¡De acuerdo, Jack!", null);
        builder.show();
    }
    
    private void msgBox(String message) {
    	Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    private void msgBox(String message, String reportMsg) {
    	TextView report = (TextView) findViewById(R.id.report);
    	Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    	report.setText(reportMsg);
    }
    
    private void setPreference (String preference, String value) {
        SharedPreferences.Editor editor = (SharedPreferences.Editor) getSharedPreferences(PREFS_NAME,0).edit();
        editor.putString(preference, value);
        editor.commit();
    }

    private void setPreference (String preference, int value) {
        SharedPreferences.Editor editor = (SharedPreferences.Editor) getSharedPreferences(PREFS_NAME,0).edit();
        editor.putInt(preference, value);
        editor.commit();
    }

    private String loadStringPreference (String preference) {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,0); 
        return prefs.getString(preference, null);
    }

    private int loadIntPreference (String preference) {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,0);
        return prefs.getInt(preference, -1);
    }

    private void removePreference (String preference) {
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME,0);
        prefs.edit().remove(preference.toString()).commit();
    }

}


class Post {
	static final String tag = "DoorMatic";

    //private InputStream is = null;
    //private String answer = "";


    void conectaPost(final String url, final String username, final String password) {
        (new Thread() {

        	public HttpClient getNewHttpClient() {
        	     try {
        	         KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        	         trustStore.load(null, null);

        	         SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
        	         sf.setHostnameVerifier(SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
        	         //sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

        	         HttpParams params = new BasicHttpParams();
        	         HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        	         HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

        	         SchemeRegistry registry = new SchemeRegistry();
        	         registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        	         registry.register(new Scheme("https", sf, 443));

        	         ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

        	         return new DefaultHttpClient(ccm, params);
        	     } catch (Exception e) {
        	         return new DefaultHttpClient();
        	     }
        	}

            @Override
            public void run() {
                try {
                    //String url = "http://192.168.1.103/door/";
                    HttpClient httpclient = getNewHttpClient();

                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("action", "open"));
                    nameValuePairs.add(new BasicNameValuePair("UserName", username));
                    nameValuePairs.add(new BasicNameValuePair("Passwd", password));
                    nameValuePairs.add(new BasicNameValuePair("portable", ""));

                    //Log.d(tag, "URL: " + url);
                    //Log.d(tag, "User: " + username);
                    UrlEncodedFormEntity form;

                    form = new UrlEncodedFormEntity(nameValuePairs, "UTF-8");
                    HttpPost httppost = new HttpPost(url);
                    httppost.setEntity(form);

                    HttpResponse response = (HttpResponse) httpclient.execute(httppost);
                    //Log.d(tag, "Reciviendo respuesta");
                    HttpEntity resEntity = response.getEntity();
                    String answer = EntityUtils.toString(resEntity);
                    Log.i(tag, "Srv-Response:\n" + answer);

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (ClientProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
             }
        }).start();
		
    }
}


